var language = ['nl-NL'];
var stringMap = [
  {
    English: 'finance and control - hobbyist programmer',
    Dutch: 'bedrijfseconoom - hobbyprogrammeur'
  }
];

document.addEventListener('DOMContentLoaded', setWebsiteLocale());

function setWebsiteLocale(setLanguage) {
  var browserLanguage =
    setLanguage === undefined ? navigator.language : setLanguage;
  var stringMapKey =
    language.indexOf(browserLanguage) === -1 ? 'English' : 'Dutch';

  document
    .querySelectorAll('[language]')
    .forEach((node, key) => (node.textContent = stringMap[key][stringMapKey]));
}
