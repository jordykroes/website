var gulp = require('gulp');
var sass = require('gulp-sass');
var del = require('del');
var browserSync = require('browser-sync').create();

gulp.task('watch', ['clean', 'build', 'browserSync'], () => {
  gulp.watch('./src/sass/**/*', ['sass', 'syncReload']);
  gulp.watch(
    ['./src/**/*', '!./src/sass/**/*', '!./src/sass'],
    ['transfer', 'syncReload']
  );
});

gulp.task('clean'),
  () => {
    return del('./dist');
  };

gulp.task('build', ['sass', 'transfer'], () => {
  return;
});

gulp.task('sass', () => {
  return gulp
    .src('./src/sass/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('transfer', () => {
  var files = ['./src/**/*', '!./src/sass/**/*', '!./src/sass'];
  return gulp.src(files).pipe(gulp.dest('./dist'));
});

gulp.task('browserSync', () => {
  return startBrowserSync();
});

gulp.task('syncReload', () => {
  return browserSync.reload();
});

function startBrowserSync() {
  browserSync.init({
    files: ['./dist/**/*'],
    server: {
      baseDir: './dist'
    },
    watchOptions: {
      ignored: './node_modules/*',
      ignoreInitial: true
    }
  });
}
