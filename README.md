# Website

The backbones of my personal website.<br>
For a live preview [click here](https://jordykroes.nl).

## Copyright and License
Based on the [Freelancer](http://startbootstrap.com/template-overviews/freelancer/) theme
powered by [Bootstrap](http://getbootstrap.com/).
